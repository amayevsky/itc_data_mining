import requests
import re
from bs4 import BeautifulSoup
import pandas as pd

import id_collection
import api_query
import date_edits
import SQL_queries
import logging
from conf import username
from conf import pw
from conf import prt
from conf import database

logger = logging.getLogger(__name__)


def grab_all_details(game_ids, to_scrape={'title', 'release date', 'price', 'genre', 'developer', 'publisher'}):
    """Function collects all details pertinent to a game one by one and inserts each new game info to the
    game details SQL database"""
    games_info = pd.DataFrame(columns=to_scrape)
    for count, game_id in enumerate(game_ids):
        game_info = grab_game_info(game_id, to_scrape, count, len(game_ids))
        if game_info != 'redirected':
            SQL_queries.insert_game_details(game_info, username, pw, prt, database)

            # Add genre info to genre table
            if 'genre' in game_info:
                for genre in game_info['genre']:
                    SQL_queries.insert_genre({'game_id': game_info['game_id'], 'genre': genre})

            # Add time-to-beat info to time-to-beat table
            if isinstance(game_info['time_to_beat'], dict):
                for k, v in game_info['time_to_beat'].items():
                    SQL_queries.insert_time_to_beat({'game_id': game_info['game_id'], 'playthrough_type': k, 'time_to_beat': v})

            # Log last time game was updated
            SQL_queries.edit_update_info(game_info, username=username, pw=pw, prt=prt, database=database)
    return games_info


def grab_game_info(game_id, to_scrape, count, total):
    """Function collects and returns the details of a single game"""
    logging.info('Retrieving... id = {} \t Progress count: {} / {}'.format(game_id, count + 1, total))
    base_url = 'https://store.steampowered.com/app/'
    url = base_url + game_id
    try:
        r = requests.get(url)
    except Exception as err:
        logging.exception(err)
        return 'redirected'

    game_info = {}

    if id_collection.check_page_status(r, url):
        if r.url == 'https://store.steampowered.com/' or 'https://store.steampowered.com/agecheck/app' in r.url:
            return 'redirected'
        soup = BeautifulSoup(r.content, 'html.parser')

        # Step 1 - Add game_id
        game_info['game_id'] = game_id

        # Step 2 - Pull developer, title and genre information
        game_info = pull_title_genre_dev(soup, to_scrape, game_info)

        # Step 3 - Pull title, price, and release date
        game_info = pull_release_date_price(to_scrape, soup, game_info)

        # Step 4 - Enrich data with API (adds data such as time-to-beat, hype and popularity from IDGB API)
        game_info.update(api_query.api_query(game_info['title']))

    return game_info


def pull_release_date_price(to_scrape, soup, game_info):
    """Pull release date and price information from a game website"""
    the_dict = {'title': 'apphub_AppName', 'release date': 'date'}

    if 'price' in to_scrape:
        if soup.find_all('div', {'class': 'game_purchase_price price'}) != []:
            the_dict['price'] = 'game_purchase_price price'
        else:
            the_dict['price'] = 'discount_original_price'

    # Find with beautiful soup
    for the_item in to_scrape:
        if the_item in the_dict:
            try:
                game_info[the_item] = soup.find_all('div', {'class': the_dict[the_item]})[0].text.strip()
            except IndexError:
                game_info[the_item] = 'Unlisted'

    game_info['release date'] = game_info['release date'].replace(',', '')
    game_info['release date'] = date_edits.reformat_date(game_info['release date'])

    # Clean price info
    game_info = clean_price(game_info)

    return game_info


def pull_title_genre_dev(soup, to_scrape, game_info):
    """Function pulls title, genre and developer information for a specific game"""
    # Find details block with all the information
    potential_details_block = soup.find_all('div', {'class': 'details_block'})

    the_details_dict = {'title': 0, 'genre': 1, 'developer': 2, 'publisher': 3}

    # Ensure correct details block pulled
    if len(potential_details_block) is not 0:
        for i in range(len(potential_details_block)):
            regex = re.compile('<div class="details_block"')
            the_reg = re.search(regex, str(potential_details_block[i]))
            if the_reg is not None:
                break
    else:
        i = 0

    # game_info = {}
    # details_block = potential_details_block[i].text.strip().split('\n')
    # for j in range(len(details_block)):
    #     if 'Title:' in details_block[j]:
    #         game_info['title'] = details_block[j][7:]
    #     elif 'Genre:' in details_block[j]:
    #         game_info['genre'] = details_block[j][7:]
    #     elif 'Developer:' == details_block[j]:
    #         game_info['developer'] = details_block[j + 1]
    #     elif 'Publisher:' == details_block[j]:
    #         game_info['publisher'] = details_block[j + 1]
    #
    # for the_item in the_details_dict:
    #     if the_item in to_scrape:
    #         if the_item not in game_info or game_info[the_item] == ['']:
    #             game_info[the_item] = [None]


    regex = re.compile('Title: (.*?)[\n]Genre: (.*?)[\n]+Developer:\n(.*?)[\n]+P?u?b?l?i?s?h?e?r?:?\n?(.*?)?\n')
    # try:
    try:
        details_block = list(re.match(regex, potential_details_block[i].text.strip()).groups())

        # Found with regex previously and now added to the game_info list
        for the_item in the_details_dict:
            if the_item in to_scrape:
                game_info[the_item] = details_block[the_details_dict[the_item]].split(", ")
                # Deal with case that publisher is not found
                if game_info[the_item] == ['']:
                    game_info[the_item] = [None]

        for item in ['title', 'developer', 'publisher']:
            game_info[item] = game_info[item][0]
        # game_info['title'] = game_info['title'][0]

    except Exception as err:
        logging.exception(err)
        for item in ['title', 'developer']:
            game_info[item] = None
        game_info['publisher'] = [None]

    return game_info


def clean_price(game_info):
    """Function cleans the price information for game info"""
    if 'Free' in game_info['price']:
        game_info['price'] = 0
    elif 'Unlisted' == game_info['price']:
        game_info['price'] = None
    else:
        game_info['price'] = game_info['price'][1:]

    return game_info
