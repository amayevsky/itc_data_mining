from datetime import datetime
import logging

logger = logging.getLogger(__name__)

def reformat_date(text):
    """
    Takes string formatted dates that have any of a series of different date-like formats and returns a relevant datetime object.
    """
    the_formats = ['%d %b %Y', '%d %B %Y', '%b %d %Y', '%B %d %Y',
                   '%d %b, %Y', '%d %B, %Y', '%b %d, %Y', '%B %d, %Y',
                   '%Y', '%d %b', '%d %B', '%b %d', '%B %d']
    for i in range(len(the_formats)):
        try:
            the_date = datetime.strptime(text.lstrip('Posted: '), the_formats[i])
            if i > 8:
                the_date = the_date.replace(year=datetime.now().year)
            return the_date
        except ValueError:
            pass#this function will raise a valueerror if the format that is currently being tested does not apply
    return None


def reformatting_date_works():
    dummyitems = [
        {'game_id': 1, 'release date': '1 Dec 2018'},
        {'game_id': 2, 'release date': '02 Dec, 2018'},
        {'game_id': 3, 'release date': '3 December, 2018'},
        {'game_id': 4, 'release date': '04 December 2018'},

        {'game_id': 5, 'release date': 'Dec 5 2018'},
        {'game_id': 6, 'release date': 'Dec 06, 2018'},
        {'game_id': 7, 'release date': 'December 7, 2018'},
        {'game_id': 8, 'release date': 'December 08 2018'},

        {'game_id': 9, 'release date': 'Posted: 9 Dec 2018'},
        {'game_id': 10, 'release date': 'Posted: 10 December, 2018'},
        {'game_id': 11, 'release date': 'Posted: Dec 11, 2018'},
        {'game_id': 12, 'release date': 'Posted: December 12 2018'},

        {'game_id': 13, 'release date': '13 Dec'},
        {'game_id': 14, 'release date': '14 December'},
        {'game_id': 15, 'release date': 'Dec 15'},
        {'game_id': 16, 'release date': 'December 16'},

        {'game_id': 17, 'release date': 'Posted: 17 Dec'},
        {'game_id': 18, 'release date': 'Posted: 18 December'},
        {'game_id': 19, 'release date': 'Posted: Dec 19'},
        {'game_id': 20, 'release date': 'Posted: December 20'},

        {'game_id': 21, 'release date': '2018'},
        {'game_id': 22, 'release date': 'Posted: 2018'},
    ]

    trash_items = [{'game_id': 23, 'release date': 'Coming Soon'}]

    for obj in dummyitems:
        assert isinstance(reformat_date(obj['release date']), datetime)

    assert reformat_date(trash_items[0]['release date']) is None

    return True


if __name__ == "__main__":
    assert reformatting_date_works() is True
