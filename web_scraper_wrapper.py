import click
import ITC_data_mining


@click.group()
def cli():
    pass


@click.command()
@click.option('--test_start', default=0, type=int, help="For testing, index of the first game to scrape")
@click.option('--test_end', default=None, type=int, help="For testing, index of the last game to scrape")
@click.option('--test_num_pages', default=None, type=int, help="For testing, how many pages of recent games to scrape")
def scrape(test_start=0, test_end=None, test_num_pages=None):
    if test_end is None and test_start == 0 and test_num_pages is None:
        click.echo('\nWill perform complete scrape of steam')

    elif test_end is not None:
        if test_start != 0:
            click.echo('\nTEST: Will scrape games %d to %d' % (test_start, test_end))
        else:
            click.echo('\nTEST: Will scrape up to game %d' % test_end)

    elif test_start != 0:
        click.echo('\nTEST: Will scrape from game %d' % test_start)

    if test_num_pages is not None:
        click.echo('\nTEST: Will scrape only first %d pages of steams' % test_num_pages)

    ITC_data_mining.main(test_start, test_end, test_num_pages)
    click.echo('\n######### Scrape complete #########')


@click.command()
@click.argument('arg1')
def f2(arg1):
    print('f2', arg1)


@click.command()
@click.argument('arg1')
@click.argument('arg2')
def f3(arg1, arg2):
    print('f3', arg1, arg2)


@click.command()
@click.argument('args', nargs=-1)
def f4(args):
    print('f4', args)


cli.add_command(scrape)
cli.add_command(f2)
cli.add_command(f3)
cli.add_command(f4)


def main():
    """

    """
    return


if __name__ == '__main__':
    cli()
