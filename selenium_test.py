import os
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.chrome.options import Options

display = Display(visible=0,size=(1024, 768))
display.start()

chrome_options = Options()
chrome_options.add_argument('--headless')
chrome_options.add_argument('--no-sandbox')
chrome_options.add_argument('--disable-dev-shm-usage')

chromedriver_path = os.path.abspath('chromedriver')

driver = webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)

# driver.get('https://www.google.com')
# driver.page_source