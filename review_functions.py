import re
import pandas as pd
from bs4 import BeautifulSoup

import selenium_queries
import SQL_queries
import date_edits
import logging
from conf import username
from conf import pw
from conf import prt
from conf import database


logger = logging.getLogger(__name__)


def pull_game_reviews(game_id, count, total, last_review=None, to_scrape={'hours before review','recommend', 'number who found helpful',
                                          'reviewer name', 'date posted'}):
    """Function collects newly posted reviews for games"""
    # Progress report
    # logging.info('in pull game reviews', game_id,count,total,last_review,to_scrape)
    logging.info('Updating reviews for id = {} \t Progress count: {} / {}'.format(game_id, count + 1, total))

    # Step 1 - Load page and check if was redirected
    driver = selenium_queries.initialise_webdriver(game_id)
    # logging.info('pull game reveiws driver test 1', type(driver), driver)
    if driver.current_url == 'https://store.steampowered.com/' or 'review' not in driver.current_url:
        selenium_queries.close_sel_webdriver(driver)
        # logging.info('redirection problems')
        return None, 0

    # logging.info('pull game reveiws driver test 2', type(driver),driver)

    # Step 2 - Pull reviews for a page. Requires scrolling down periodically to load more reviews
    reviews_info = pd.DataFrame(columns=to_scrape)
    num_new_reviews = 0
    end = False
    # logging.info('pull game reviews variable test', reviews_info,num_new_reviews,end)
    while end is False:
        # Scroll down page to load more reviews
        content, end = selenium_queries.scroll_down(driver)
        soup = BeautifulSoup(content, 'html.parser')

        # Populate the reviews dataframe with visible reviews on page
        # logging.info('pre collect reviews', type(soup), num_new_reviews, game_id, to_scrape, last_review, reviews_info, end)
        reviews_info, end = collect_reviews(soup, num_new_reviews, game_id, to_scrape, last_review, reviews_info, end)
        num_new_reviews = len(reviews_info)

        # # Stop at
        # if num_new_reviews > 1000:

    # Step 3 - Close web-driver once all reviews on page were found
    selenium_queries.close_sel_webdriver(driver)

    # Progress report
    logging.info(str(num_new_reviews) + ' new reviews found')

    return reviews_info, num_new_reviews


def populate_review_db(num_new_reviews, reviews_info):
    """Populates the reviews database with new reviews found for a game"""
    # Insert one by one into an sql database
    if num_new_reviews != 0:
        # Needs to be inserted in reverse order such that can correctly find the most recent
        # review for following scrapes
        for row in range(reviews_info.shape[0] - 1, -1, -1):
            review = reviews_info.loc[row].to_dict()
            # Insert into database
            SQL_queries.insert_game_reviews(review, username=username, pw=pw, prt=prt, database=database)


def grab_all_reviews(game_ids, to_scrape={'hours before review','recommend', 'number who found helpful',
                                          'reviewer name', 'date posted'}):
    for count, game_id in enumerate(game_ids):
        # If game has already been scraped - collect the reviewer name of the last review scraped
        if SQL_queries.check_id_exists(game_id, username=username, pw=pw, prt=prt, database=database):
            last_review = SQL_queries.retrieve_first_review(game_id, username=username, pw=pw, prt=prt, database=database)
        else:
            last_review = None

        # Step 1 - Collect reviews
        # logging.info('this:', len(game_ids))
        # logging.info('and this:',game_id, count, len(game_ids), last_review, to_scrape)
        reviews_info, num_new_reviews = pull_game_reviews(game_id, count, len(game_ids), last_review, to_scrape)
        # logging.info('this happened:', reviews_info, num_new_reviews)

        # Step 2 - Enter all new reviews into a database
        if reviews_info is not None:
            populate_review_db(num_new_reviews, reviews_info)

            # Step 3 - Update the log table
            SQL_queries.edit_update_info({'game_id':game_id},
                                     username=username, pw=pw, prt=prt, database=database)
        # Progress report
        if reviews_info is None:
            logging.info('Page was redirected')


def clean_recommended(review):
    """Cleans the recommended parameter of the review info"""
    # Change recommended to Boolean (with integer representation)
    if review['recommend'] == 'Recommended':
        review['recommend'] = 1
    else:
        review['recommend'] = 0

    return review


def clean_hours_helpful(review):
    """Cleans the hours before review and number who found helpful data from reviews"""
    for item in ['hours before review', 'number who found helpful']:
        if item == 'hours before review':
            value = re.findall('[0-9]+\.[0-9]+', review[item])
        else:
            value = re.findall('[0-9]+', review[item])
        if value != []:
            review[item] = value[0]
        else:
            review[item] = 0

    return review


def clean_review_data(review):
    """Functions cleans the review data pulled from the website for the SQL database"""
    review = clean_hours_helpful(review)
    review = clean_recommended(review)
    # Change date to a datetime
    review['date posted'] = date_edits.reformat_date(review['date posted'])
    return review


def is_repeat_review(review, last_review):
    """Checks if review has already been scraped"""
    if review['reviewer name'] == last_review:
        return True

    return False


def collect_single_review(review_block, game_id, to_scrape):
    the_dict = {'reviewer name': 'apphub_CardContentAuthorName offline ellipsis',
                'hours before review': 'hours', 'recommend': 'title', 'number who found helpful': 'found_helpful',
                'date posted': 'date_posted'}

    # Collect info for single review of game
    review = {'game_id': game_id}
    for the_item in to_scrape:
        if the_item in the_dict:
            try:
                tmp = review_block.find_all('div', class_=the_dict[the_item])[0].text.strip()
                review[the_item] = tmp
            except IndexError:
                if the_item == 'reviewer name':
                    try:
                        tmp = \
                            review_block.find_all('div', class_='apphub_CardContentAuthorName online ellipsis')[
                                0].text.strip()
                        review[the_item] = tmp
                    except IndexError:
                        review[the_item] = 'Unlisted'
                else:
                    review[the_item] = 'Unlisted'

    return review


def collect_reviews(soup, num_new_reviews, game_id, to_scrape, last_review, reviews_info, end):
    """Adds review information for a game into a pandas dataframe. Returns the dataframe"""
    # Find block of reviews
    # logging.info('in collect reviews', type(soup), num_new_reviews,game_id,to_scrape,last_review,reviews_info,end)

    reviews_block = soup.find_all('div', class_="apphub_Card modalContentLink interactable")[num_new_reviews:]
    if reviews_block is None:
        return None, True

    # Find details per review with beautiful soup
    for review_block in reviews_block:
        # Step 1 - Collect review info
        review = collect_single_review(review_block, game_id, to_scrape)

        # Step 2 - Check if review has been seen before
        if is_repeat_review(review, last_review):
            end = True
            break

        # Step 3 - Clean review info for database
        # review['date posted'] = review['date posted'].replace(',', '')
        review = clean_review_data(review)

        # Step 4 - Save review if not empty
        if review != {}:
            reviews_info = reviews_info.append(review, ignore_index=True, sort=True)

    return reviews_info, end
