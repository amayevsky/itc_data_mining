import requests
import json
import logging

logger = logging.getLogger(__name__)


def api_query(the_game):
    """Enriches the data of a game using the IGDB API"""
    apikey = '827ff89c21e842c0ae3a4fec704acdca'

    url_root = 'https://api-endpoint.igdb.com/games/?search='
    url_tail = '&fields=name,hypes,popularity,time_to_beat,platforms,rating,pegi'

    try:
        r = requests.get(url_root + the_game + url_tail, headers={'user-key': apikey})
    except Exception as err:
        logging.exception(err)

    the_info = json.loads(r.text)

    the_results = {}
    if len(the_info) == 0:
        i = 0
        the_info = [[]]

    else:
        try:
            for i in range(len(the_info)):
                if the_game == the_info[i]['name']:
                    del (the_info[i]['id'])
                    del (the_info[i]['name'])
                    break
        except Exception as err:
            logging.exception(err)

    if 'hypes' not in the_info[i]:
        the_results['hypes'] = None
    else:
        the_results['hypes'] = the_info[i]['hypes']

    if 'popularity' not in the_info[i]:
        the_results['popularity'] = None
    else:
        the_results['popularity'] = the_info[i]['popularity']

    if 'time_to_beat' not in the_info[i]:
        the_results['time_to_beat'] = None
    else:
        the_results['time_to_beat'] = the_info[i]['time_to_beat']

    if 'platforms' not in the_info[i]:
        the_results['platforms'] = None
    else:
        the_results['platforms'] = len(the_info[i]['platforms'])

    if 'rating' not in the_info[i]:
        the_results['rating'] = None
    else:
        the_results['rating'] = the_info[i]['rating']

    if 'pegi' not in the_info[i]:
        the_results['pegi'] = None
    else:
        the_results['pegi'] = the_info[i]['pegi']['rating']

    return the_results
