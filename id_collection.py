import requests
import re
import time
import math
from bs4 import BeautifulSoup

import SQL_queries
import logging
from conf import username
from conf import pw
from conf import prt
from conf import database
from conf import num_games_per_page


logger = logging.getLogger(__name__)


def check_page_status(r, url):
    for iteration in range(5):
        if r.status_code != 200:
            time.sleep(iteration * 60)
            requests.get(url)
        else:
            return True


def calc_page_count(start, end, desired_pages):
    """If the scrape is run for testing, this function calculate the subset of pages needed to scrape of the website"""
    # Calc number of pages needed (only if testing options used)
    if end is not None:
        page_count = math.ceil((end - start) / num_games_per_page)
        if desired_pages is not None:
            desired_pages = min(page_count, desired_pages)
        else:
            desired_pages = page_count
        # Progress statement
        logging.info('Therefore, number of pages to be looked at during '
                     'scrape due to options chosen = {}\n'.format(desired_pages))
    return desired_pages


def find_last_page_num(soup):
    """Find the number of pages needed to scrape in order to scrape the whole website"""
    num_pages = soup.find_all('div', class_='search_pagination_right')
    num_pages = num_pages[0].find_all('a')
    num_pages = [int(i.text) for i in num_pages if i.text.isdigit()]
    num_pages = num_pages[num_pages.index(max(num_pages))]
    logging.info('Total number of pages to scrape = {}\n'.format(num_pages))
    return num_pages


def page_ids(count, soup, game_ids, last_game_id, repeat_id, desired_pages):
    """Collects all game IDs on a given webpage. Returns an updated list of game_ids and a flag indicating if
     a game that already exists in the database was found"""
    # Progress report
    logging.info('Collecting IDs on page {} / {}'.format(count, desired_pages))

    # Collect all ids on given page
    useful = soup.prettify()
    id_expression = re.compile('data-ds-appid="([0-9]*)')
    game_ids.extend(re.findall(id_expression, useful))

    # Check if some of the page has already been scraped before
    if last_game_id in game_ids:
        repeat_id = True

        # Progress report
        logging.info('\nAll game IDs up-to the most recent game in the database have been collected')

        # Retain only un-scraped games in list
        index_last_game = game_ids.index(last_game_id)
        game_ids = game_ids[:index_last_game]

    return game_ids, repeat_id


def collect_ids(start=0, end=None, desired_pages=None, first_scrape=False, test=False):
    """Function returns a list of all game_IDs on steam which are not already in the database"""
    # Step 1 - Initialise values
    last_page = False
    url = 'https://store.steampowered.com/search/?sort_by=Released_DESC&category1=998'
    # count = 1
    repeat_flag = False
    game_ids = []

    # collects the ids backwards if this is the first scrape
    if first_scrape:
        curr_url = url + '&page=1'
        r = requests.get(curr_url)
        soup = BeautifulSoup(r.content, 'html.parser')
        last_page_num = find_last_page_num(soup)
        count = last_page_num
    else:
        count = 1

    # Step 2 - If options were used for testing, calculate
    # desired number of pages according to the combination of options used
    if test:
        desired_pages = calc_page_count(start, end, desired_pages)

    # Step 3 - Find the last retrieved game from the previous scrape so to know when to stop scraping
    last_game_id = SQL_queries.retrieve_last_game(username=username, pw=pw, prt=prt, database=database)

    # Step 4 - Find all new game_IDs
    while last_page is False and repeat_flag is False:
        curr_url = url + '&page=' + str(count)
        r = requests.get(curr_url)

        # Check if page loaded
        if check_page_status(r, url):

            # Convert html to soup object
            soup = BeautifulSoup(r.content, 'html.parser')

            # Find total number of pages to scrape if wasn't defined with the testing parameters
            if desired_pages is None:
                desired_pages = find_last_page_num(soup)

            # Collect all the game IDs for one page
            game_ids, repeat_flag = page_ids(count, soup, game_ids, last_game_id, repeat_flag, desired_pages)

            # Increment the page count
            if first_scrape:
                SQL_queries.insert_update_info(game_ids[::-1], username=username, pw=pw, prt=prt, database=database)
                game_ids = []
                count -= 1
                if count < last_page_num - desired_pages:
                    last_page = True
            else:
                count += 1

                # Check if last page desired or the last possible on steam was just scraped
                if count > desired_pages:
                    last_page = True

    # step 5 - Reverse order of list most recent game inserted last in SQL table
    if not first_scrape:
        SQL_queries.insert_update_info(game_ids[::-1], username=username, pw=pw, prt=prt, database=database)
    # game_ids = game_ids[::-1]

    # return game_ids
