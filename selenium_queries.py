import os
from pyvirtualdisplay import Display
from selenium.webdriver.chrome.options import Options
from selenium import webdriver
import time
import logging
from conf import AWS
from conf import WEBDRIVER_PATH

logger = logging.getLogger(__name__)


def create_sel_webdriver(WEBDRIVER_PATH):
    """Function creates a selenium webdriver without a chrome window opening.
    Also has functionality to run on AWS instance"""
    if AWS:
        display = Display(visible=0, size=(1024, 768))
        display.start()

    chrome_options = Options()
    chrome_options.add_argument('--headless')

    if AWS:
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-dev-shm-usage')

    chromedriver_path = os.path.abspath(WEBDRIVER_PATH)

    return webdriver.Chrome(executable_path=chromedriver_path, chrome_options=chrome_options)


def close_sel_webdriver(driver):
    """Function closes the selenium webdriver"""
    driver.close()


def pull_html_source(driver, url):
    """Function pulls the html source of a website from selenium"""
    driver.get(url)


def scroll_down(driver):
    """Function scrolls down the page once until the end of the page"""
    end = False
    # Get scroll height.
    last_height = driver.execute_script("return document.body.scrollHeight")

    # Scroll down to the bottom.
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

    # Wait to load the page.
    time.sleep(1)

    # Calculate new scroll height and compare with last scroll height.
    new_height = driver.execute_script("return document.body.scrollHeight")
    if new_height == last_height:
        end = True

    return driver.page_source, end


def initialise_webdriver(game_id):
    """Initialises and returns the web-driver for review scraping for a particular game."""
    # Step 1 - Initialise review dataframe and webdriver
    the_url = 'https://steamcommunity.com/app/'
    review_url = '/reviews/?browsefilter=mostrecent&snr=1_5_100010_&filterLanguage=all&p=1'
    full_url = the_url + game_id + review_url

    # Step 2 - Initialise Chrome web_driver
    driver = create_sel_webdriver(WEBDRIVER_PATH)

    # Step 3 - Pull html source code from url
    pull_html_source(driver, full_url)

    return driver
