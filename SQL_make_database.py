import mysql.connector

username = 'root'
pw = 'Sillypassword1!'
prt = 3306
database = 'steam_db'


def create_databases(username=username, pw=pw, prt=prt, database=database):
    make_database(username, pw, prt, database)
    make_tables(username, pw, prt, database)


def make_database(username, pw, prt, database):
    con = mysql.connector.connect(user=username, password=pw, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
                    """
                    CREATE DATABASE IF NOT EXISTS %s
                    """ % (database,))
        # print(cur.fetchall())
    except:
        con.close()
    con.close()


def make_tables(username, pw, prt, database):
    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS games
                (id int NOT NULL AUTO_INCREMENT,
                game_id int NOT NULL UNIQUE,
                name varchar(255) NOT NULL,
                genre varchar(255),
                price varchar(255),
                developer varchar(255),
                publisher varchar(255),
                release_date varchar(255),
                platforms int,
                time_to_beat varchar(255),
                popularity FLOAT,
                hypes FLOAT,
                pegi INT,
                rating FLOAT,
                PRIMARY KEY (id)
                )
            """)

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS reviews
                (id int NOT NULL AUTO_INCREMENT,
                game_id int NOT NULL,
                reviewer_name varchar(255) NOT NULL,
                hours_played varchar(255),
                recommendation TINYINT(1),
                number_helpful int,
                review_date varchar(255) NOT NULL,
                PRIMARY KEY (id)
                )
            """)
    except:
        con.close()
    con.close()


## delete from here to end

def retrieve_last_game(username=username, pw=pw, prt=prt, database=database):
    """Query SQL database to retrieve most recent game pulled (first in database). Return an empty string
    if database is empty"""
    pass


def insert_game_details(game_info, username=username, pw=pw, prt=prt, database=database):
    """Function inserts a row of game details to SQL game details database"""
    insert_string = (str(game_info['game_id']), game_info['title'], game_info['genre'], game_info['price'],
                     game_info['developer'], game_info['publisher'], game_info['release date'], game_info['platforms'],
                     game_info['time_to_beat'], game_info['popularity'], game_info['hypes'], game_info['pegi'],
                     game_info['rating'])

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
        INSERT INTO games (
            game_id,
            name,
            genre,
            price,
            developer,
            publisher,
            release_date,
            platforms,
            time_to_beat,
            popularity,
            hypes,
            pegi,
            rating
        ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """

    try:

        cur.execute(the_query, insert_string)
        con.commit()
        # cur.execute(
        #     """
        #     SELECT * FROM games
        #     """)
        # print(cur.fetchall())
    except:
        con.close()
    con.close()


def insert_game_reviews(review_info, username=username, pw=pw, prt=prt, database=database):
    """Function inserts a row of a game review to SQL reviews database"""
    insert_string = (str(game_info['game_id']), game_info['title'], game_info['genre'], game_info['price'],
                     game_info['developer'], game_info['publisher'], game_info['release date'])

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
        INSERT INTO games (
            game_id,
            name,
            genre,
            price,
            developer,
            publisher,
            release_date
        ) VALUES {}
        """.format(insert_string)

    try:

        cur.execute(the_query)
        con.commit()
        # cur.execute(
        #     """
        #     SELECT * FROM games
        #     """)
        # print(cur.fetchall())
    except:
        con.close()
    con.close()


def retrieve_last_review(game_id):
    """Query SQL review database to retrieve most recent review pulled (first in database)."""
    pass


def check_id_exists():
    """Query SQL review database to return True or False based on whether the game has been scraped before"""
    return False


def order_review_db():
    """Function that orders the SQL database correctly so that the most recent review for each game is first"""
    pass


def all_game_ids():
    """Function that queries the SQL game database and returns all the game IDs"""
    insert_string = [str(game_info.game_id), game_info.title, game_info.genre, game_info.price, game_info.developer, game_info.publisher, game_info.release_date]

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            SELECT game_id FROM games
            """)

        # print(cur.fetchall())
    except:
        con.close()
    con.close()
    return cur.fetchall()
