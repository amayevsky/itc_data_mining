import SQL_queries
import game_functions
import review_functions
import id_collection
import logging
import sys

from conf import username
from conf import pw
from conf import prt
from conf import database
from conf import NUM_GAMES_TO_UPDATE
from conf import NUM_GAMES_TO_UPDATE_DETAILS


# import matplotlib
# matplotlib.use('agg')


def main(start=0, end=None, desired_pages=None):
    try:
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)

        # create a file handler
        file_handler = logging.FileHandler('web_scrape.log')
        file_handler.setLevel(logging.INFO)

        # create a logging format
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler.setFormatter(formatter)

        # add the handlers to the logger
        logger.addHandler(file_handler)
        logger.addHandler(logging.StreamHandler(sys.stdout))  # For printing to stdout

        # Step 0 - Check if a test scrape:
        test = False
        if start != 0 or end is not None or desired_pages is not None:
            test = True

        # Step 1 - Create the databases required
        first_scrape = not SQL_queries.database_exists(username=username, pw=pw, prt=prt, database=database)
        if first_scrape:
            SQL_queries.create_databases(username, pw, prt, database)

        # Step 2 - Collect new game IDs
        logging.info('\n###################################################\n')
        logging.info('Step 1: Collect IDs for all unseen games')
        id_collection.collect_ids(start, end, desired_pages, first_scrape, test)
        new_game_ids = SQL_queries.find_unscraped_games(NUM_GAMES_TO_UPDATE_DETAILS, username=username, pw=pw, prt=prt, database=database)

        # Step 3 - Collect information for the game_ids we pulled
        logging.info('\n###################################################\n')
        logging.info('Step 2: Scrape game details for all unseen games')
        logging.info('Number of new games to scrape: {}\n'.format(len(new_game_ids)))
        game_functions.grab_all_details(new_game_ids)

        # Step 4 - Collect the review information for each game_id,
        games_to_update = SQL_queries.retrieve_unupdated(NUM_GAMES_TO_UPDATE, username, pw, prt, database)
        logging.info('\n###################################################\n')
        logging.info('Step 3: Scrape game reviews for both new and old games')
        logging.info('Number of games to scrape reviews for = {}\n'.format(len(games_to_update)))
        review_functions.grab_all_reviews(games_to_update)
    except Exception as err:
        logging.exception(err)


if __name__ == '__main__':
    main()
