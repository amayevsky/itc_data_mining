import mysql.connector
import logging
# import pandas as pd

from conf import username
from conf import pw
from conf import prt
from conf import database


logger = logging.getLogger(__name__)


def create_databases(username=username, pw=pw, prt=prt, database=database):
    make_database(username, pw, prt, database)
    make_tables(username, pw, prt, database)


def make_database(username, pw, prt, database):
    con = mysql.connector.connect(user=username, password=pw, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
                    """
                    CREATE DATABASE IF NOT EXISTS %s
                    """ % (database,))
        # print(cur.fetchall())
    except:
        con.close()
    con.close()


def make_tables(username, pw, prt, database):
    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS updates
                (id int NOT NULL AUTO_INCREMENT,
                game_id int NOT NULL UNIQUE,
                last_update TIMESTAMP NULL DEFAULT NULL,
                PRIMARY KEY (id)
                )
            """)

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS games
                (id int NOT NULL AUTO_INCREMENT,
                game_id int NOT NULL UNIQUE,
                name varchar(255) NOT NULL,
                price FLOAT,
                developer varchar(255),
                publisher varchar(255),
                release_date DATETIME,
                platforms int,
                popularity FLOAT,
                hypes FLOAT,
                pegi INT,
                rating FLOAT,
                PRIMARY KEY (id),
                FOREIGN KEY (game_id) REFERENCES updates(game_id)
                )
            """)

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS reviews
                (id int NOT NULL AUTO_INCREMENT,
                game_id int,
                reviewer_name varchar(255) NOT NULL,
                hours_played varchar(255),
                recommendation TINYINT(1),
                number_helpful int,
                review_date DATETIME,
                PRIMARY KEY (id),
                FOREIGN KEY (game_id) REFERENCES updates(game_id)
                )
            """)

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS genres
                (id int NOT NULL AUTO_INCREMENT,
                game_id int NOT NULL,
                genre varchar(255) NOT NULL,
                PRIMARY KEY (id),
                FOREIGN KEY (game_id) REFERENCES updates(game_id)
                )
            """)

        cur.execute(
            """
            CREATE TABLE IF NOT EXISTS time_to_beat
                (id int NOT NULL AUTO_INCREMENT,
                game_id int NOT NULL,
                playthrough_type varchar(255),
                time_to_beat FLOAT,
                PRIMARY KEY (id),
                FOREIGN KEY (game_id) REFERENCES updates(game_id)
                )
            """)

    except:
        con.close()
    con.close()


def retrieve_last_game(username=username, pw=pw, prt=prt, database=database):
    """Query SQL database to retrieve most recent game pulled (first in database). Return an empty string
    if database is empty"""

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            SELECT game_id FROM updates ORDER BY id DESC LIMIT 1
            """)

        last_id = str(cur.fetchall()[0][0])

    except:
        last_id = None

    con.close()
    return last_id


def find_unscraped_games(the_limit, username=username, pw=pw, prt=prt, database=database):
    """Returns a list of the games whose details have not been scraped."""
    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
            SELECT game_id
            FROM updates
            WHERE last_update is NULL
            ORDER BY id
            LIMIT %s"""

    try:
        cur.execute(the_query, (the_limit,))

        # print(cur.fetchall())
        results = cur.fetchall()
    except Exception as err:
        logging.exception(err)
        # print(err)
        con.close()
    con.close()

    return [str(elmt[0]) for elmt in results]


def insert_game_details(game_info, username=username, pw=pw, prt=prt, database=database):
    """Function inserts a row of game details to SQL game details database"""
    try:
        insert_string = (str(game_info['game_id']), game_info['title'], game_info['price'], game_info['developer'],
                         game_info['publisher'], game_info['release date'], game_info['platforms'],
                         game_info['popularity'], game_info['hypes'], game_info['pegi'],
                         game_info['rating'])

        con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

        cur = con.cursor()
        the_query = """
            INSERT INTO games (
                game_id,
                name,
                price,
                developer,
                publisher,
                release_date,
                platforms,
                popularity,
                hypes,
                pegi,
                rating
            ) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """

        cur.execute(the_query, insert_string)
        con.commit()
        # cur.execute(
        #     """
        #     SELECT * FROM games
        #     """)
        # print(cur.fetchall())
    except Exception as err:
        logging.exception(err)
        con.close()
    con.close()


def insert_game_reviews(review_info, username=username, pw=pw, prt=prt, database=database):
    """Function inserts a row of a game review to SQL reviews database"""
    insert_string = (str(review_info['game_id']), review_info['reviewer name'], review_info['hours before review'],
                     review_info['recommend'], review_info['number who found helpful'], review_info['date posted'])

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
        INSERT INTO reviews (
            game_id,
            reviewer_name,
            hours_played,
            recommendation,
            number_helpful,
            review_date
        ) VALUES (%s, %s, %s, %s, %s, %s)
        """

    try:

        cur.execute(the_query, insert_string)
        con.commit()

    except:
        pass

    con.close()


def insert_many_game_reviews(review_info, username=username, pw=pw, prt=prt, database=database):
    """Function inserts a dataframe containing review info into the sql database.
    NOTE: this function is not currently called anywhere
    """

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    try:
        review_info.to_sql(con=con, name='review', if_exists='append')

        # cur.execute(the_query, insert_string)
        # con.commit()

    except Exception as err:
        logging.exception(err)

    con.close()


def insert_genre(game_info, username=username, pw=pw, prt=prt, database=database):
    """Function inserts a row of game details to SQL game details database"""
    insert_string = (game_info['game_id'], game_info['genre'])

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
        INSERT INTO genres (
            game_id,
            genre
        ) VALUES (%s, %s)
        """

    try:

        cur.execute(the_query, insert_string)
        con.commit()
        # cur.execute(
        #     """
        #     SELECT * FROM games
        #     """)
        # print(cur.fetchall())
    except:
        con.close()
    con.close()

def insert_time_to_beat(game_info, username=username, pw=pw, prt=prt, database=database):
    """Inserts time-to-beat stats into the table"""
    insert_string = (game_info['game_id'], game_info['playthrough_type'], game_info['time_to_beat'])

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
        INSERT INTO time_to_beat (
            game_id,
            playthrough_type,
            time_to_beat
        ) VALUES (%s, %s, %s)
        """

    try:

        cur.execute(the_query, insert_string)
        con.commit()
        # cur.execute(
        #     """
        #     SELECT * FROM games
        #     """)
        # print(cur.fetchall())
    except Exception as err:
        logging.exception(err)
        con.close()
    con.close()


def insert_update_info(id_list, username=username, pw=pw, prt=prt, database=database):
    """Function inserts a row indicating the time when reviews were last scraped for a game id"""
    # insert_string = (game_info['game_id'], )
    # the format required for executemany is a list of lists
    to_insert = [[elmt] for elmt in id_list]

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
        INSERT INTO updates 
        (game_id) 
        VALUES (%s)
        ON DUPLICATE KEY UPDATE game_id = game_id"""

    try:

        cur.executemany(the_query, to_insert)
        con.commit()
        # cur.execute(
        #     """
        #     SELECT * FROM games
        #     """)
        # print(cur.fetchall())
    except Exception as err:
        logging.exception(err)
        con.close()
    con.close()


def edit_update_info(game_info, username=username, pw=pw, prt=prt, database=database):
    """Function edits a row of indicating when reviews were last scraped for a game id"""
    insert_string = (game_info['game_id'], )

    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    the_query = """
        UPDATE updates
        SET last_update = now()
        WHERE game_id = %s
        """

    try:

        cur.execute(the_query, insert_string)
        con.commit()
        # cur.execute(
        #     """
        #     SELECT * FROM games
        #     """)
        # print(cur.fetchall())
    except Exception as err:
        logging.exception(err)
        con.close()
    con.close()


def retrieve_unupdated(number_of_ids, username=username, pw=pw, prt=prt, database=database):
    """Get a list of game ids from which the reviews have been updated longest ago."""
    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            SELECT game_id
            FROM updates
            WHERE last_update is not NULL
            ORDER BY last_update ASC
            LIMIT %s
            """ % (number_of_ids,))

        # print(cur.fetchall())
        game_ids = cur.fetchall()
    except:
        game_ids = None

    con.close()
    result = [str(el[0]) for el in game_ids]
    return result


def retrieve_first_review(game_id, username=username, pw=pw, prt=prt, database=database):
    """Query SQL review database to retrieve most recent review pulled (last in database)."""
    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            SELECT reviewer_name
            FROM reviews
            WHERE game_id=%s
            ORDER BY id
            DESC LIMIT 1
            """ % (game_id,))

        # print(cur.fetchall())
        last_review = cur.fetchall()[0][0]
    except:
        last_review = None

    con.close()

    return last_review


def check_id_exists(game_id, username=username, pw=pw, prt=prt, database=database):
    """Query SQL review database to return True or False based on whether the game has been scraped before"""
    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            SELECT game_id
            FROM reviews
            WHERE game_id=%s
            """ % (game_id,))

        # print(cur.fetchall())
        tmp = len(cur.fetchall())
        found = (tmp != 0)

    except:
        con.close()
        return False

    else:
        con.close()
        return found


def order_review_db(username=username, pw=pw, prt=prt, database=database):
    """Function that orders the SQL database correctly so that the most recent review for each game is first"""
    pass


def all_game_ids(username=username, pw=pw, prt=prt, database=database):
    """Function that queries the SQL game database and returns all the game IDs"""
    con = mysql.connector.connect(user=username, password=pw, database=database, port=prt)

    cur = con.cursor()
    try:
        cur.execute(
            """
            SELECT game_id
            FROM games
            ORDER BY id
            """)
        result = cur.fetchall()
        # print(cur.fetchall())
    except:
        con.close()
    con.close()
    result = [str(el[0]) for el in result]
    return result

def database_exists(database=database,username=username, pw=pw, prt=prt):
    """ Checks if the database exists, returns true if exists."""
    con = mysql.connector.connect(user=username, password=pw, port=prt)

    cur = con.cursor()
    the_query = """
            SELECT schema_name
            FROM information_schema.schemata
            WHERE schema_name = %s"""

    try:
        cur.execute(the_query, (database,))

        # print(cur.fetchall())
        results = cur.fetchall()
        if len(results) is 0:
            return False
        else:
            return True
    except Exception as err:
        # logging.exception(err)
        print(err)
        con.close()
    con.close()

